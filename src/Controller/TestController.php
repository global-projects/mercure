<?php
namespace App\Controller;

use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends  AbstractController
{
    /**
     * @Route("/new-order")
     */
    public function newOrder(HubInterface $hub)
    {
        $em=$this->getDoctrine()->getManager();
            $order = new Order();
            $order
                ->setName("deneme")
                ->setStatus(1);
            $em->persist($order);
            $em->flush();
            $event = new Update(
                [
                    ("1"."/"."getOrders")
                ],
                json_encode([
                    "@EventType"=>"create",
                    "id"=>$order->getId(),
                    "name"=>$order->getName(),
                    "status"=>$order->getStatus(),
                ])
            );
            $hub->publish($event);
            exit("bitti");
    }
}


//$event = new Update(
//    [
//        "order"
//    ],
//    json_encode([
//        "type"=>"create",
//        "id"=>$order->getId(),
//        "name"=>$order->getName(),
//        "status"=>$order->getStatus(),
//    ])
//);
//$hub->publish($event);