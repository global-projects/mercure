"use strict";
let orders = [
    {
        id:1,
        name:"deneme"
    },
    {
        id:2,
        name:"deneme"
    },
    {
        id:3,
        name:"deneme"
    },
];
const orderDiv=document.getElementById("order");

(function () {
    const endpoint = 'localhost:3000';
    const baseUrl ='http://' + endpoint + '/.well-known/mercure';
    const url = new URL(baseUrl);

// Add topic to listen to
    url.searchParams.append('topic', "1"+"/"+"getOrders");
    const eventSource = new EventSource(url);

// The callback will be called every time an update is published
    eventSource.onmessage = function (event) {
        let data= JSON.parse(event.data);
        console.log(data);
        if (data["@EventType"]==="create"){
            orders.push({
                id:data.id,
                name:data.name,
            });
            getOrders();
        }
    };
    getOrders();
})();

function getOrders() {
    orderDiv.innerHTML = "";
    orders.map(function (value){
        orderDiv.innerHTML += `<p>${value.name}-${value.id}</p>`;
    })
}